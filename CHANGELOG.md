# Changelog

All notable changes to `optidist/oditor` will be documented in this file

## 0.0.2 - 2020-07-06

- Add traits to integrate withthe  AbstrActions, `optidist/abstractions`, package. 

## 0.0.1 - 2020-07-04

- Initial release.
