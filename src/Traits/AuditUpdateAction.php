<?php

namespace Jit\Oditr\Traits;

use Illuminate\Database\Eloquent\Model;
use Jit\Oditr\Contracts\Auditable;
use Jit\Oditr\Models\Audit;

trait AuditUpdateAction
{
    public function audit(Model $model, array $data = [])
    {
        if ($model instanceof Auditable) {
            Audit::logUpdate($model);
        }
    }
}
