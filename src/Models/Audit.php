<?php

namespace Jit\Oditr\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Auth;
use Jit\Oditr\Contracts\Auditable;

class Audit extends Model
{
    protected static $actor;

    protected $fillable = [
        "actor_id", "actor_type", "action", "auditable_id", "auditable_type",
        "ip_address", "user_agent", "extra"
    ];

    public function auditable(): MorphTo
    {
        return $this->morphTo("auditable");
    }

    protected static function authenticate()
    {
        self::$actor = Auth::user();
    }

    public static function log(
        string $action,
        ?Auditable $entity = null,
        array $extra = []
    ) {
        self::authenticate();

        $data = [
            "actor_id" => self::$actor ? self::$actor->id : null,
            "actor_type" => self::$actor ? self::$actor->getMorphClass() : null,
            "action" => $action,
            "ip_address" => request()->ip(),
            "user_agent" => request()->userAgent(),
            "extra" => count($extra) ? json_encode($extra) : null
        ];

        if ($entity) {
            $data["auditable_id"] = $entity->getKey();
            $data["auditable_type"] = $entity->getMorphClass();
        }

        Audit::create($data);
    }

    public static function logRegister(?Auditable $entity, array $extra = [])
    {
        self::log("register", $entity, $extra);
    }

    public static function logLogin(array $extra = [])
    {
        self::log("login", null, $extra);
    }

    public static function logLogout(array $extra = [])
    {
        self::log("logout", null, $extra);
    }

    public static function logView(?Auditable $entity = null, array $extra = [])
    {
        self::log("view", $entity, $extra);
    }

    public static function logCreate(?Auditable $entity = null, array $extra = [])
    {
        self::log("create", $entity, $extra);
    }

    public static function logUpdate(?Auditable $entity = null, array $extra = [])
    {
        self::log("update", $entity, $extra);
    }

    public static function logDelete(?Auditable $entity = null, array $extra = [])
    {
        self::log("delete", $entity, $extra);
    }
}
