<?php

namespace Jit\Oditr;

use Illuminate\Support\ServiceProvider;

class OditrServiceProvider extends ServiceProvider
{
    protected string $migrations_path = __DIR__ . "/Database/Migrations";

    public function boot()
    {
         $this->loadMigrationsFrom($this->migrations_path);
    }

    public function register()
    {
        //
    }
}
