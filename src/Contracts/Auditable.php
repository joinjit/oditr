<?php

namespace Jit\Oditr\Contracts;

interface Auditable
{
    public function getKey();

    public function getMorphClass();
}
