# Oditr

Oditr is a package that adds simple database auditing to your Laravel project.

## Installation

You can install the package via composer:

```bash
composer require jit/oditr
```

Publish the migrations and config file.

```bash
php artisan vendor:publish --provider="Jit\Oditr\OditrServiceProvider"
```

Run the migrations to create the `audits` table.

```bash
php artisan migrate
```

## Usage

Below is a quick guide on how to use Oditr.

### Auditing an Action

Oditr allows you to audit any action in the app. The log contains the following:
* Action (string to represent the performed action).
* Actor (the user that performed the action).
* Auditable (the entity which the action was performed on).
* IP Address (fetched from the request).
* User Agent (fetched from the request).
* Extra Data (key value array).

To use the `Audit` class, you must:
1. Use `Audit` from `Jit\Oditr\Models\Audit;

#### Note on Auditables
In order to log the entity which the action was performed on, it must implement the `Auditable` interface.

### Quick Example

In the example below, we will log an action which represents a user paying for a product.

``` php
// Product.php

[...]
use Jit\Oditr\Contracts\Auditable;

class Product extends Model implements Auditable
{
    // The auditable method are implemented inside the Model class.
    [...]
}

// ProductController.php

[...]
use Jit\Oditr\Models\Audit;

class ProductController
{
    [...]
    public function pay(Product $product)
    {
        $user = Auth::user();

        $user->pay($product->amount);

        Audit::log("pay", $product, [ "amount" => $product->amount ]);
    }
}
```

### Audit Helpers

Audit helpers are used to simplify logging CRUD operations.

#### Log View

The log view will add a new log with the `view` action.

```php
Audit::logView($auditable, [ "extra" => "data" ]);
```

#### Log Create

The log create will add a new log with the `create` action.

```php
Audit::logCreate($auditable, [ "extra" => "data" ]);
```

#### Log Update

The log update will add a new log with the `update` action.

```php
Audit::logUpdate($auditable, [ "extra" => "data" ]);
```

#### Log Delete

The log delete will add a new log with the `delete` action.

```php
Audit::logDelete($auditable, [ "extra" => "data" ]);
```

#### Log Register

The log register will add a new log with the `register` action. Usually, the user in this case is the `auditable` entity that is being  created. The actor will be set to `null`.

```php
Audit::logRegister($registered_user, [ "extra" => "data" ]);
```

#### Log Login

The log login will add a new log with the `login` action where no `auditable` entity is required. In this case, make sure that the `logLogin(array $extra = [])` method is called **after** authenticated the user.

```php
Audit::logLogin([ "extra" => "data" ]);
```

#### Log Logout

The log logout will add a new log with the `logout` action where no `auditable` entity is required. In this case, make sure that the `logLogout(array $extra = [])` method is called **before** logging out the authenticated user.

```php
Audit::logLogout([ "extra" => "data" ]);
```

### Auditing Abstract Actions
Oditr provides traits to audit actions from the `optidist/abstractions` package.
After enabling audit logging on the action itself, you can use the traits to automatically log the action instead of manually 
implementing the `audit(Model $model, array $data = [])` method.

#### Example
```php
// ConcreteCreateAction.php
[...]

use Jit\Oditr\Traits\AuditCreateAction;

class ConcreteCreateAction extends AbstractCreateAction implements AuditAfter
{
    use AuditCreateAction;

    protected function create(array $data = []): Model
    {
        //
    }
}
```

Similarly, you can use `AuditUpdateAction` and `AuditDeleteAction` for update and delete actions respectively.

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

### Security

If you discover any security related issues, please email j.karam@joinjit.com instead of using the issue tracker.

## Credits

- [Joe Karam](https://github.com/joekaram)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
