<?php

namespace Jit\Oditr\Tests\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Jit\Oditr\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    protected $fillable = [
        "name"
    ];
}
