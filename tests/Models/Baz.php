<?php

namespace Jit\Oditr\Tests\Models;

use Illuminate\Database\Eloquent\Model;

class Baz extends Model
{
    protected $fillable = [
        "name"
    ];
}
