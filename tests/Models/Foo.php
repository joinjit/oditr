<?php

namespace Jit\Oditr\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Jit\Oditr\Contracts\Auditable;

class Foo extends Model implements Auditable
{
    protected $fillable = [
        "name"
    ];
}
