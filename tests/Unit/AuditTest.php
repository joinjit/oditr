<?php

namespace Jit\Oditr\Tests\Unit;

use Jit\Oditr\Models\Audit;
use Jit\Oditr\Tests\LogsInAsUser;
use Jit\Oditr\Tests\Models\Baz;
use Jit\Oditr\Tests\Models\Foo;
use Jit\Oditr\Tests\TestCase;
use TypeError;

class AuditTest extends TestCase
{
    use LogsInAsUser;

    /** @test */
    public function it_persists_audits_to_database()
    {
        $data = [
            "actor_id" => 1,
            "actor_type" => "ActorType",
            "action" => "action",
            "auditable_id" => 2,
            "auditable_type" => "AuditableType",
            "ip_address" => "1.2.3.4",
            "user_agent" => "TheAgent"
        ];

        Audit::create($data);

        $this->assertDatabaseHas("audits", $data);
    }

    /** @test */
    public function it_logs_auditables()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::log("create", $auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "create",
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_does_not_log_non_auditables()
    {
        $this->expectException(TypeError::class);

        $non_auditable = Baz::create([ "name" => "Bar" ]);

        Audit::log("create", $non_auditable);
    }

    /** @test */
    public function it_logs_actor_if_authenticated()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        $user = $this->login();

        Audit::log("create", $auditable);

        $this->assertAuthenticatedAs($user);
        $this->assertDatabaseHas("audits", [
            "action" => "create",
            "actor_id" => $user->id,
            "actor_type" => $user->getMorphClass(),
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_does_not_log_actor_if_unauthenticated()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::log("create", $auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "create",
            "actor_id" => null,
            "actor_type" => null,
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_adds_extra_data_as_string()
    {
        $extra = [ "key" => "value" ];

        Audit::log("create", Foo::create([ "name" => "Bar" ]), $extra);

        $audit = Audit::first();

        $this->assertJsonStringEqualsJsonString(json_encode($extra), $audit->extra);
    }

    /** @test */
    public function it_stores_extra_as_null_if_empty()
    {
        Audit::log("create", Foo::create([ "name" => "Bar" ]));

        $audit = Audit::first();

        $this->assertNull($audit->extra);
    }
}
