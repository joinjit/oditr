<?php

namespace Jit\Oditr\Tests\Unit;

use Jit\Oditr\Models\Audit;
use Jit\Oditr\Tests\LogsInAsUser;
use Jit\Oditr\Tests\Models\Foo;
use Jit\Oditr\Tests\Models\User;
use Jit\Oditr\Tests\TestCase;

class AuditHelpersTest extends  TestCase
{
    use LogsInAsUser;

    /** @test */
    public function it_logs_with_the_log_register_helper()
    {
        $user = User::create([ "name" => "Registered User" ]);

        Audit::logRegister($user);

        $this->assertDatabaseHas("audits", [
            "action" => "register",
            "auditable_id" => $user->id,
            "auditable_type" => $user->getMorphClass()
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_login_helper()
    {
        $user = $this->login();

        Audit::logLogin();

        $this->assertDatabaseHas("audits", [
            "action" => "login",
            "actor_id" => $user->id,
            "actor_type" => $user->getMorphClass(),
            "auditable_id" => null,
            "auditable_type" => null
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_logout_helper()
    {
        $user = $this->login();

        Audit::logLogout();

        $this->assertDatabaseHas("audits", [
            "action" => "logout",
            "actor_id" => $user->id,
            "actor_type" => $user->getMorphClass(),
            "auditable_id" => null,
            "auditable_type" => null
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_view_helper()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::logView($auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "view",
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_create_helper()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::logCreate($auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "create",
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_update_helper()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::logUpdate($auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "update",
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }

    /** @test */
    public function it_logs_with_the_log_delete_helper()
    {
        $auditable = Foo::create([ "name" => "Bar" ]);

        Audit::logDelete($auditable);

        $this->assertDatabaseHas("audits", [
            "action" => "delete",
            "auditable_id" => $auditable->id,
            "auditable_type" => $auditable->getMorphClass()
        ]);
    }
}
