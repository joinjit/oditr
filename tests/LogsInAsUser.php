<?php

namespace Jit\Oditr\Tests;

use Jit\Oditr\Tests\Models\User;

trait LogsInAsUser
{
    public function login(): User
    {
        $user = User::create([ "name" => "My Name"]);

        $this->be($user);

        return $user;
    }
}
