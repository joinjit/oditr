<?php

namespace Jit\Oditr\Tests;

use Illuminate\Database\Schema\Blueprint;
use Jit\Oditr\OditrServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    protected function getPackageProviders($app)
    {
        return [ OditrServiceProvider::class ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', "testbench");
        $app['config']->set("database.connections.testbench", [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function setUpDummyDatabases(): void
    {
        $this->app["db"]->connection()
            ->getSchemaBuilder()
            ->create("foos", function (Blueprint $table) {
                $table->increments("id");
                $table->string("name");
                $table->timestamps();
            });

        $this->app["db"]->connection()
            ->getSchemaBuilder()
            ->create("bazs", function (Blueprint $table) {
                $table->increments("id");
                $table->string("name");
                $table->timestamps();
            });

        $this->app["db"]->connection()
            ->getSchemaBuilder()
            ->create("users", function (Blueprint $table) {
                $table->increments("id");
                $table->string("name");
                $table->timestamps();
            });
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->setUpDummyDatabases();

        $this->artisan("migrate", ["--database" => "testbench"])->run();
    }
}
